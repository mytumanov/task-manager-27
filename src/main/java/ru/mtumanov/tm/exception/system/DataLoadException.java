package ru.mtumanov.tm.exception.system;

public class DataLoadException extends AbstractSytemException {

    public DataLoadException() {
        super("ERROR! Data load exception!");
    }

}
