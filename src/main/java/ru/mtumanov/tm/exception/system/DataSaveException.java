package ru.mtumanov.tm.exception.system;

public class DataSaveException extends AbstractSytemException {
    
    public DataSaveException() {
        super("ERROR! Data save exception!");
    }
    
}
