package ru.mtumanov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.mtumanov.tm.api.repository.ICommandRepository;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.repository.IUserRepository;
import ru.mtumanov.tm.api.service.*;
import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.command.data.AbstractDataCommand;
import ru.mtumanov.tm.command.data.DataBase64LoadCommand;
import ru.mtumanov.tm.command.data.DataBinaryLoadCommand;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.system.ArgumentNotSupportedException;
import ru.mtumanov.tm.exception.system.CommandNotSupportedException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.model.User;
import ru.mtumanov.tm.repository.CommandRepository;
import ru.mtumanov.tm.repository.ProjectRepository;
import ru.mtumanov.tm.repository.TaskRepository;
import ru.mtumanov.tm.repository.UserRepository;
import ru.mtumanov.tm.service.*;
import ru.mtumanov.tm.util.SystemUtil;
import ru.mtumanov.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PROJECT_COMMANDS = "ru.mtumanov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    @Getter
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService, propertyService);

    {
        @NotNull final Reflections reflections = new Reflections(PROJECT_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes)
            registry(clazz);
    }

    private void initData() {
        try {
            final boolean checkBinary = Files.exists(Paths.get(AbstractDataCommand.FILE_BINARY));
            if (checkBinary) processCommand(DataBinaryLoadCommand.NAME, false);
            if (checkBinary) return;
            final boolean checkBase64 = Files.exists(Paths.get(AbstractDataCommand.FILE_BASE64));
            if (checkBase64) processCommand(DataBase64LoadCommand.NAME, false);
        } catch (@NotNull AbstractException e) {
            loggerService.error(e);
        }
    }

    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(fileName), pid.getBytes());
        } catch (@NotNull final IOException e) {
            loggerService.error(e);
        }
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(@Nullable final String[] args) {
        initLogger();
        initDemo();
        initPID();
        initData();

        processArguments(args);
        while (Thread.currentThread().isAlive()) {
            System.out.println("ENTER COMMAND:");
            try {
                @NotNull final String cmd = TerminalUtil.nextLine();
                processCommand(cmd);
                System.out.println("OK");
                loggerService.command(cmd);
            } catch (final Exception e) {
                System.out.println("FAIL");
                loggerService.error(e);
            }
        }
    }

    private void initDemo() {
        try {
            @NotNull final Project p1 = new Project("Test Project", "Project for test", Status.IN_PROGRESS);
            @NotNull final Project p2 = new Project("Complete Project", "Project with complete status", Status.COMPLETED);
            @NotNull final Project p3 = new Project("FKSKDJ#&$&*@(!!111", "Strange project", Status.NOT_STARTED);
            @NotNull final Project p4 = new Project("11112222", "Project with numbers", Status.IN_PROGRESS);

            @NotNull final Task t1 = new Task("NAME1", "description for task");

            projectService.add(p1);
            projectService.add(p2);
            projectService.add(p3);
            projectService.add(p4);

            taskService.add(t1);
            taskService.add(new Task("NAME2", "description2"));
            taskService.add(new Task("NAME3", "description3"));

            userService.create("COOL_USER", "cool", Role.ADMIN);
            @NotNull final User u2 = userService.create("NOT_COOL_USER", "Not Cool", "notcool@email.ru");
            userService.create("123qwe!", "vfda4432!", "vfda4432@email.ru");

            p1.setUserId(u2.getId());
            p2.setUserId(u2.getId());
            t1.setUserId(u2.getId());

        } catch (final AbstractException e) {
            System.out.println("ERROR! Fail to initialize test data.");
            System.out.println(e.getMessage());
        }
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0 || args[0] == null)
            return;
        try {
            processArgument(args[0]);
            System.exit(0);
        } catch (final AbstractException e) {
            System.out.println(e.getMessage());
        }
    }

    private void processArgument(@NotNull String arg) throws AbstractException {
        if (arg.isEmpty())
            return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null)
            throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    private void processCommand(@NotNull final String cmd) throws AbstractException {
        if (cmd.isEmpty())
            return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(cmd);
        if (abstractCommand == null)
            throw new CommandNotSupportedException(cmd);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public void processCommand(@NotNull final String command, boolean checkRoles) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        try {
            final AbstractCommand command = clazz.getDeclaredConstructor().newInstance();
            registry(command);
        } catch (ReflectiveOperationException e) {
            loggerService.error(e);
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK_MANAGER **");
        Runtime.getRuntime()
                .addShutdownHook(new Thread(() -> loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **")));
    }

}
