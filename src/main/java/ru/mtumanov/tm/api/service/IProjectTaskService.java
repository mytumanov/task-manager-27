package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;

public interface IProjectTaskService {

    void bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) throws AbstractException;

    void removeProjectById(@NotNull String userId, @NotNull String projectId) throws AbstractException;

    void unbindTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) throws AbstractException;

}
