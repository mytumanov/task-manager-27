package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Remove project by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Project project = getProjectService().findOneById(id);
        @NotNull final String userId = getUserId();
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

}
