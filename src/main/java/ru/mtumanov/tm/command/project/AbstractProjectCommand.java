package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.api.service.IProjectTaskService;
import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void showProject(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus());
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

}
