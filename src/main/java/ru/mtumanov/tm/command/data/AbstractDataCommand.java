package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.dto.Domain;

import javax.annotation.Nullable;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setProjects(getServiceLocator().getProjectService().findAll());
        domain.setTasks(getServiceLocator().getTaskService().findAll());
        domain.setUsers(getServiceLocator().getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null)
            return;
        serviceLocator.getUserService().set(domain.getUsers());
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getAuthService().logout();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

}
