package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.Domain;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.system.DataSaveException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-bin";

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to binary file.";
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SAVE DATA BINARY]");
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_BINARY);
            @NotNull final Path path = file.toPath();
            Files.deleteIfExists(path);
            Files.createFile(path);
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(domain);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (@NotNull IOException e) {
            throw new DataSaveException();
        }
    }

}
