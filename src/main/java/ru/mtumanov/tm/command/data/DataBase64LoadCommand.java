package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.Domain;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.system.DataLoadException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from base64 file.";
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD BASE64]");
        try {
            @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BASE64));
            @Nullable final String base64Data = new String(base64Byte);
            @Nullable final byte[] bytes = Base64.getDecoder().decode(base64Data);
            @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            objectInputStream.close();
            byteArrayInputStream.close();
            setDomain(domain);
        } catch (@NotNull IOException | @NotNull ClassNotFoundException e) {
            throw new DataLoadException();
        }
    }

}
