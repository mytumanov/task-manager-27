package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.Domain;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.system.DataLoadException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from binary file.";
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA BINARY LOAD]");
        try {
            @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            setDomain(domain);
        } catch (@NotNull IOException | @NotNull ClassNotFoundException e) {
            throw new DataLoadException();
        }
    }

}
