package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.Domain;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.system.DataSaveException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;

public class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-svae-base64";

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to base64 file.";
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA BASE64 SAVE]");
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        @NotNull final Path path = file.toPath();

        try {
            Files.deleteIfExists(path);
            Files.createFile(path);

            @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(domain);
            objectOutputStream.close();
            byteArrayOutputStream.close();

            @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
            @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);

            fileOutputStream.write(base64.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (@NotNull IOException e) {
            throw new DataSaveException();
        }
    }

}
