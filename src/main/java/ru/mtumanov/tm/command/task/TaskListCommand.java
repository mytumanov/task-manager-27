package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.enumerated.TaskSort;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Show list tasks";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-list";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        @NotNull final String userId = getUserId();
        Comparator<Task> comparator = null;
        if (sort != null)
            comparator = sort.getComparator();
        @NotNull final List<Task> tasks = getTaskService().findAll(userId, comparator);
        renderTask(tasks);
    }

}
